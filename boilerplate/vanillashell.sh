#!/bin/sh

JAVA=/usr/bin/java

JVM_ARGS="--add-modules java.se --add-exports java.base/jdk.internal.ref=ALL-UNNAMED \
--add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/java.nio=ALL-UNNAMED \
--add-opens java.base/sun.nio.ch=ALL-UNNAMED --add-opens java.management/sun.management=ALL-UNNAMED \
--add-opens jdk.management/com.sun.management.internal=ALL-UNNAMED"

$JAVA $JVM_ARGS -Dcolorterm -classpath "./lib/*" id.nosh.vanilla.Main
