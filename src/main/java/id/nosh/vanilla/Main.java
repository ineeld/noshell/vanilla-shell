/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.vanilla;

import java.util.logging.LogManager;

import org.slf4j.bridge.SLF4JBridgeHandler;

import id.nosh.shell.Nosh;
import id.scanpath.ScanScope;

/**
 * @author indroneel
 *
 */

public class Main {

	public static void main(String[] args) throws Exception {

		System.setProperty("java.util.logging.manager", VanillaLogManager.class.getName());
		//setup slf4j as the underlying logging mechanism.
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();

		ScanScope scope = new ScanScope().includePackage(Main.class.getPackageName());
		Nosh nosh = new Nosh()
			.scanCommand(scope)
			.setBanner("banner.txt");
		nosh.start();

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				try {
					//ss.stop();
				}
				catch(Exception exep) {
					exep.printStackTrace();
				}
				finally {
					LogManager lm = LogManager.getLogManager();
					if(lm instanceof VanillaLogManager) {
						((VanillaLogManager) lm).nowReset();
					}
				}
			}
		};
		Runtime.getRuntime().addShutdownHook(new Thread(runnable));
	}
}
