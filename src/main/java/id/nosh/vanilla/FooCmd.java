/*
 * Copyright (c) The original author or authors
 *
 * This file is part of a software distribution. You may not use this file except in compliance with
 * the terms and conditions that are applicable for the distribution. These terms and conditions are
 * detailed out in a file named LEGAL.TXT or LEGAL.MD which is available as part of the original
 * distribution.
 *
 * If you have received this file without the associated terms and conditions details, you must do
 * either one of the following:
 *
 *   1. Stop using this file, and delete all copies from each and every media that you own.
 *
 *   2. Write to us at license@indroneel.com with details such as file name, distribution name and
 *      release version, and we will provide you with a copy of the terms and conditions that are
 *      applicable for this file.
 */

package id.nosh.vanilla;

import java.util.Optional;

import id.nosh.ext.Command;
import id.nosh.ext.CommandOption;
import id.nosh.ext.Description;
import id.nosh.ext.DisplayName;
import id.nosh.ext.GroupBy;
import id.nosh.ext.MainMethod;
import id.nosh.ext.Mandatory;

/**
 * @author indroneel
 *
 */

@Command({"foo"})
@Description("For testing purposes only")
public class FooCmd {

	@CommandOption(simple = "c", full = "create")
	@Description("Create a new key-value entity")
	@GroupBy("operation")
	@Mandatory
	public void setCreateOpt(@DisplayName("key") String keyName, @DisplayName("value") String keyValue) {
		System.out.printf("create option with %s and %s\n", keyName, keyValue);
	}

	@CommandOption(simple = "u", full = "update")
	@Description("Update an existing key-value entity")
	@GroupBy("operation")
	@Mandatory
	public void setUpdateOpt(String keyName, String keyValue) {
		System.out.printf("update option with %s and %s\n", keyName, keyValue);
	}

	@MainMethod
	public void execute(@DisplayName("extra") int arg1) {
		System.out.println(arg1);
	}
}
